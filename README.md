# Rust-Ketos repl

This is a quick extension of the [ketos](https://github.com/murarth/ketos) repl so that it can load rust-based dynamic libraries. It is not particularly safe, it assumes that all functions in a loaded dynamic library that don't start with "_" are either of the right form or will not be called. I'm not it's "safe" with respect to rusts (lack of) guarantees on calling convention even if everything is done perfectly.

`completion.rs` and `main.rs` were copied with minor changes from the ketos repository.

## Usage

```
cargo build
target/debug/repl
(use target/debug/libtestlib.so :all)
(test_func 1 2 4)
```
