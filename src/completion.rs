//! Performs name-based text completion using the current `GlobalScope`.

use ketos::scope::{Scope, MasterScope};
use rustyline::completion::{Completer, extract_word};
use rustyline::error::ReadlineError as Error;

use std::collections::BTreeSet;

pub struct ScopeCompleter<'a> {
    scope: &'a Scope,
    break_chars: BTreeSet<char>,
}

static BREAK_CHARS: [char; 5] = [' ', '\t', '(', ')', '\n'];

impl<'a> ScopeCompleter<'a> {
    pub fn new(scope: &'a Scope) -> ScopeCompleter<'a> {
        ScopeCompleter {
            scope: scope,
            break_chars: BREAK_CHARS.iter().cloned().collect(),
        }
    }
}

impl<'a> Completer for ScopeCompleter<'a> {
    fn complete(&self, text: &str, pos: usize) -> Result<(usize, Vec<String>), Error> {
        let text = &text[..pos];
        let (start, text) = extract_word(text, pos, &self.break_chars);

        // Don't attempt to complete when the input is empty
        if text.chars().all(|c| c.is_whitespace()) {
            return Ok((0, vec![]));
        }

        let scope = self.scope;
        let mut results = Vec::new();

        for name in MasterScope::get_names() {
            scope.with_name(name, |name| {
                if name.starts_with(text) {
                    results.push(name.to_owned());
                }
            });
        }

        scope.with_values(|values| {
            for &(name, _) in values {
                scope.with_name(name, |name| {
                    if name.starts_with(text) {
                        results.push(name.to_owned());
                    }
                });
            }
        });

        scope.with_macros(|macros| {
            for &(name, _) in macros {
                scope.with_name(name, |name| {
                    if name.starts_with(text) {
                        results.push(name.to_owned());
                    }
                });
            }
        });

        if results.is_empty() {
            Ok((start, vec![]))
        } else {
            Ok((start, results))
        }
    }
}
