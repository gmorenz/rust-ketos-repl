use elfloader::ElfBinary;
use elfloader::elf::STT_FUNC;

use ketos::compile::CompileError::ModuleError;
use ketos::error::Error;
use ketos::function::{Arity, FunctionImpl};
use ketos::module::{Module, ModuleBuilder, ModuleLoader};
use ketos::name::Name;
use ketos::scope::{GlobalScope, Scope};

use libloading::{Library, Symbol};

use libc;

use mmap::{MapOption, MemoryMap};

use std::slice;
use std::mem::{forget, swap, transmute, uninitialized};
use std::ops::Deref;
use std::path::PathBuf;

fn mmap_file(name: &str) -> (MemoryMap, u64) {
    use std::fs;
    use std::os::unix::io::AsRawFd;

    fn get_fd(file: &fs::File) -> libc::c_int {
        file.as_raw_fd()
    }

    let file = fs::OpenOptions::new()
                   .read(true)
                   .open(&name)
                   .unwrap();
    let len = file.metadata().unwrap().len();
    let fd = get_fd(&file);

    (MemoryMap::new(len as usize,
                    &[MapOption::MapReadable, MapOption::MapFd(fd)])
         .unwrap(),
     len)
}

pub struct DynLoader {
    search_paths: Vec<PathBuf>,
}

impl DynLoader {
    pub fn with_search_paths(paths: Vec<PathBuf>) -> DynLoader {
        DynLoader { search_paths: paths }
    }
}

fn dyn_load_module(sname: &str, fname: &str, scope: &Scope) -> Result<Module, Error> {
    let (map, size) = mmap_file(fname);
    let region = unsafe { slice::from_raw_parts(map.data(), size as usize) };

    // TODO: Handle Errors!
    let elffile = ElfBinary::new(fname, region).unwrap();
    let lib = Library::new(fname).unwrap();
    let scope = GlobalScope::new_using(scope);
    // TODO: What is the best name to use below?
    let mut builder = ModuleBuilder::new(sname, scope);

    {
        let bbuilder = &mut builder;
        elffile.for_each_symbol(|sym| {
            let symname = elffile.symbol_name(sym);
            if sym.sym_type().0 == STT_FUNC.0 && !symname.starts_with('_') {
                // This *unsafely* assumes the type of all functions!
                // This is unresolvable unless I figure out how to use
                // .rlib files instead of .so files
                let f: Result<Symbol<*const u8>, _> = unsafe { lib.get(symname.as_bytes()) };

                if let Ok(f) = f {
                    let f = *f.deref();
                    if !f.is_null() {
                        let f: FunctionImpl = unsafe { transmute(f) };
                        // Ugly (but memory safe) swap mechanism!
                        let mut other = unsafe { uninitialized() };
                        swap(bbuilder, &mut other);
                        other = other.add_function(symname, f, Arity::Range(0, 0xffffffff));
                        swap(bbuilder, &mut other);
                        forget(other);
                    }
                }
            }
        });
    }

    forget(lib);
    Ok(builder.finish())

}

impl ModuleLoader for DynLoader {
    fn load_module(&self, name: Name, scope: &Scope) -> Result<Module, Error> {
        let sname = scope.with_name(name, |sname| sname.to_owned());
        if sname.contains('/') {
            dyn_load_module(&sname, &sname, scope)
        } else {
            let mut path = None;
            for p in &self.search_paths {
                let mut p = p.clone();
                p.push(&sname);
                if p.exists() {
                    path = Some(p);
                    break;
                }
            }

            let fname = try!(path.ok_or(ModuleError(name)));

            dyn_load_module(&sname, fname.to_str().unwrap(), scope)
        }
    }
}
