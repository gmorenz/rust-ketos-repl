/// Test library

extern crate ketos;

use ketos::error::Error;
use ketos::function::FunctionImpl;
use ketos::integer::Integer;
use ketos::scope::Scope;
use ketos::value::Value;

#[no_mangle]
pub fn test_func(scope: &Scope, vals: &mut [Value]) -> Result<Value, Error> {
    // Type check!
    let _: FunctionImpl = test_func;

    println!("Hello, you sent: {} values", vals.len());

    Ok(Value::Integer(Integer::from_usize(vals.len())))
}
